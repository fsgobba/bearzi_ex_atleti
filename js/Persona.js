var Persona = (function () {
    function Persona(name, age) {
        this.nome = name;
        this.eta = age;
    }
    Persona.prototype.getProfile = function () {
        var out = this.nome + " è una persona di " + this.eta + " anni";
        return out;
    };
    return Persona;
}());
//# sourceMappingURL=Persona.js.map