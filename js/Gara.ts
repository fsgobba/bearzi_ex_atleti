class Gara {

    private div_descrizioni: JQuery;
    private pulsante: JQuery;
    private atleta_1: Atleta;
    private atleta_2: Atleta;
    private atleta_3: Atleta;
    private atleti: Atleta[];
    private interval_id: number;

    constructor() {
        console.log("new Gara");
        // assegno subito ad alcune variabili della classe i div che dovrò utilizzare in seguito
        this.div_descrizioni = $("#descrizioni");
        this.pulsante = $("#start input");
        this.pulsante.on("click",() => this.clickPulsante());
        // richiamo la funzione che inizializza gli atleti
        this.initAtleti();
        // richiamo la funzione che visualizza le descrizioni degli atleti
        this.fillDescrizioni();
    }

    private clickPulsante():void {
        console.log("clickPulsante");
        this.interval_id = setInterval(()=> this.updateAtleti(), 500);
        this.pulsante.off("click");
    }

    private updateAtleti():void {
        var gara_finita:boolean = false;
        for (var i:number = 0; i<this.atleti.length; i++) {
            this.atleti[i].corri(10);
            if (this.atleti[i].metri_percorsi_totali>=500) {
                gara_finita = true;
            }
        }
        if (gara_finita) {
            clearInterval(this.interval_id);
            this.pulsante.on("click", () => this.riparti());
        }
    }

    private riparti():void {
        this.reset();
        this.interval_id = setInterval( ()=>this.updateAtleti(), 500);
        this.pulsante.off("click");
    }

    private initAtleti():void {
        // dichiaro alcune variabili temporanee che verranno riutilizzate nella funzione
        var eta:number,velocita:number;
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_1 = new Atleta("Giovanni",eta,velocita,"#gara .atleta1");
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_2 = new Atleta("Andrea",eta,velocita,"#gara .atleta2");
        // genero età e velocità per l'atleta
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        // creo l'atleta
        this.atleta_3 = new Atleta("Anna",eta,velocita,"#gara .atleta3");
        // creo un'array di atleti per gestirli successivamente attraverso dei loop
        this.atleti = [this.atleta_1,this.atleta_2,this.atleta_3];
    }

    private fillDescrizioni():void {
        for (var i:number = 0; i<this.atleti.length; i++) {
            var atleta:Atleta = this.atleti[i];
            var div:JQuery = this.div_descrizioni.find(".atleta"+(i+1));
            div.html(atleta.getProfile());
        }
    }

    private generaNumeroCasuale(minimo: number, massimo: number, decimali:number = 0):number {
        var out:number = NaN;
        var fattore_decimali:number = Math.pow(10,decimali);
        var diff:number = (massimo*fattore_decimali)-(minimo*fattore_decimali);
        var numero:number = Math.random();
        var var_casuale:number = Math.round(numero*diff);
        out = (((minimo*fattore_decimali) + var_casuale)/fattore_decimali);
        return out;
    }

    private reset() {
        this.initAtleti();
        this.fillDescrizioni();
    }
}
var gara = new Gara();
